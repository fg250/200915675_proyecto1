package proyecto1_compi2;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import escaner.*;     
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.swing.JFileChooser;
//import javax.swing.JFrame;
//import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
//import javax.swing.WindowConstants;
//import javax.swing.filechooser.FileFilter;
//import javax.swing.filechooser.FileNameExtensionFilter;
//import javax.swing.*;
//import javax.swing.border.Border;
//mport archivo_escenario.*;
//import archivo_inteligencia.*;
//import java.awt.Desktop;
//import java.io.IOException;
//import javax.swing.event.CaretEvent;
//import javax.swing.event.CaretListener;
//import javax.swing.text.BadLocationException;

/**
 *
 * @author fg250
 */
public class principal extends javax.swing.JFrame {

    /**
     * Creates new form ventana_principal
     */
    public principal() {
        initComponents();
        
        
        
    }
  

    public JFileChooser filechooser = new JFileChooser();

//    public static List<ErroresLexicos> errorLexico = new ArrayList<ErroresLexicos>();
//    public static List<tabla_simbolo> tabla_sim = new ArrayList<tabla_simbolo>();

    
   
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea4 = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jEditorPane2 = new javax.swing.JEditorPane();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jScrollPane2.setViewportView(jTextArea2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jTextArea3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextArea3CaretUpdate(evt);
            }
        });
        jTextArea3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextArea3KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextArea3KeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(jTextArea3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane1.addTab("Recursos", jPanel2);

        jTextArea4.setColumns(20);
        jTextArea4.setRows(5);
        jTextArea4.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextArea4CaretUpdate(evt);
            }
        });
        jTextArea4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextArea4KeyTyped(evt);
            }
        });
        jScrollPane4.setViewportView(jTextArea4);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Escenario", jPanel3);

        jTextArea5.setColumns(20);
        jTextArea5.setRows(5);
        jTextArea5.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextArea5CaretUpdate(evt);
            }
        });
        jTextArea5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextArea5KeyTyped(evt);
            }
        });
        jScrollPane5.setViewportView(jTextArea5);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Inteligencia", jPanel4);

        jEditorPane1.setEditable(false);
        jEditorPane1.setContentType("text/html"); // NOI18N
        jScrollPane8.setViewportView(jEditorPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Tabla Simbolos", jPanel1);

        jEditorPane2.setContentType("text/html"); // NOI18N
        jEditorPane2.setText("");
        jScrollPane9.setViewportView(jEditorPane2);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Errores", jPanel5);

        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel1.setText("jLabel1");

        jMenu1.setText("Archivo");

        jMenuItem3.setText("Nuevo");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setText("Abrir");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem5.setText("Guardar");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem6.setText("Guardar como");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuItem7.setText("Salir");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Analisis");

        jMenuItem1.setText("Ejecutar analisis");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem2.setText("Mostrar errores");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        jMenu4.setText("Juego");

        jMenuItem11.setText("Vista Previa");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem11);

        jMenuItem12.setText("Editor escenario");
        jMenu4.add(jMenuItem12);

        jMenuItem13.setText("Iniciar juego");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem13);

        jMenuBar1.add(jMenu4);

        jMenu3.setText("Ayuda");

        jMenuItem8.setText("Manual tecnico");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuItem9.setText("Manual de usuario");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        jMenuItem10.setText("Acerca de");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem10);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTabbedPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed

//   
//        errorLexico.clear();
//        tabla_sim.clear();

        
        
        //COMPILANDO ARCHIVO RECURSOS
        File codigorec = new File("temp");
            try {
                        try(FileWriter escritor =  new FileWriter(codigorec)){
                        escritor.write(jTextArea3.getText());
                        escritor.close();

                        }
                        new sintactico(new lexico(new FileInputStream(codigorec))).parse();
                } catch (Exception ex) {
                    System.out.println(ex);
                } 

            System.out.println(" \n ***************FINALIZO DE COMPILAR EL ARCHIVO RECURSOS************** \n");
        
        
//        //COMPILANDO ARCHIVO ESCENARIO
//        File codigoescen = new File("temp");
//            try {
//                        try(FileWriter escritor =  new FileWriter(codigoescen)){
//                        escritor.write(jTextArea4.getText());
//                        escritor.close();
//
//                        }
//                       new sintacticoescen(new lexicoEscen(new FileInputStream(codigoescen))).parse();
//                } catch (Exception ex) {
//                    System.out.println(ex);
//                } 
//
//            System.out.println(" \n ***************FINALIZO DE COMPILAR EL ARCHIVO ESCENARIO************** \n");
//            
//            //COMPILANDO ARCHIVO INTELIGENCIA
//        File codigoint = new File("temp");
//            try {
//                        try(FileWriter escritor =  new FileWriter(codigoint)){
//                        escritor.write(jTextArea5.getText());
//                        escritor.close();
//
//                        }
//                       new sintacticoint(new lexicoInt(new FileInputStream(codigoescen))).parse();
//                } catch (Exception ex) {
//                    System.out.println(ex);
//                }
//            
//            System.out.println("\n ***************FINALIZO DE COMPILAR EL ARCHIVO INTELIGENCIA************** \n");
//            
//            
//            //CREACION DEL ARCHIVO HTML QUE CONTENDRA LA TABLA DE SIMBOLOS DEL SISTEMA
//            
//            File pagina2html = new File("C:\\tabla.html");
//        String urlDoc = "file://localhost/"+pagina2html.getAbsolutePath();
//        
//        try (FileWriter escritor = new FileWriter(pagina2html)) {
//               String datos = "<html><head></head><body><br> <center><p>TABLA DE SIMBOLOS DE LOS ARCHIVOS</p><br> <br> <br><table border = \"1\"><tr><th>ARCHIVO</th><th>DESCRIPCION</th><th>TOKEN</th><th>LEXEMA</th></tr>";
//              escritor.write(datos);
//               for(tabla_simbolo aux : tabla_sim){
//                   
//                   datos =  "<td>"+aux.archivo + "</td><td>" + aux.descripcion + "</td><td>"+aux.token + "</td><td>" +aux.lexema +"</td></tr>";
//               
//                   escritor.write(datos);
//               }
//               
//                           datos = "</table></center></body></html>";
//                             escritor.write(datos);
//                             escritor.close();
//            } catch (IOException ex) { 
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        } 
//        
//        try {
//            jEditorPane1.setPage(urlDoc);
//        } catch (IOException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }                      

    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
   
        
//        File paginahtml = new File("C:\\errores.html");
//        String urlDoc = "file://localhost/"+paginahtml.getAbsolutePath();
//        
//        try (FileWriter escritor = new FileWriter(paginahtml)) {
//               String datos = "<html><head></head><body><br> <center><p>ERRORES LEXICOS EN LOS ARCHIVOS</p><br> <br> <br><table border = \"1\"><tr><th>Descripcion del Error</th><th>Simbolo</th><th>Fila</th><th>Columna</th></tr>";
//              escritor.write(datos);
//               for(ErroresLexicos aux : errorLexico){
//                   
//                   datos =  "<td>"+aux.desc_error + "</td><td>" + aux.simbolo + "</td><td>"+aux.fila + "</td><td>" +aux.columna +"</td></tr>";
//               
//                   escritor.write(datos);
//               }
//               
//                           datos = "</table></center></body></html>";
//                             escritor.write(datos);
//                             escritor.close();
//            } catch (IOException ex) { 
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        } 
//        
//        try {
//            jEditorPane2.setPage(urlDoc);
//        } catch (IOException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        jTabbedPane1.setSelectedComponent(jPanel5);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
     
        System.exit(0);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        
        jTextArea3.setText("");    
        jTextArea4.setText("");  
        jTextArea5.setText("");  
        jEditorPane1.setText("");  
        jEditorPane2.setText("");  
        
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    
    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed

        
//        //CARGANDO EL ARCHIVO DE RECURSOS
//        
//            FileReader lector = null;
//            BufferedReader br = null;
//            filechooser.setDialogTitle("Abrir Archivo Recursos");
//            int returnVal = filechooser.showOpenDialog(null);
//            if (returnVal == JFileChooser.APPROVE_OPTION) {
//                File archivo = filechooser.getSelectedFile();
//             
//                try {
//                     //LEYENDO EL ARCHIVO DE RECURSOS
//                    lector = new FileReader(archivo);
//                    br = new BufferedReader(lector);
//                    String datos = "";
//                    String dato = "";
//                    while ((datos = br.readLine()) != null) {
//                        dato += datos + '\n';
//                    }
//                    jTextArea3.setText(dato);
//                    br.close();
//
//                    datos = "";
//                    dato = "";
//                    
//                }catch (Exception ex) {
//                } finally {
//                
//                } 
//            }
//            
//            //CARGANDO EL ARCHIVO DE ESCENARIO
//            
//            FileReader lector2 = null;
//            BufferedReader br2 = null;
//            filechooser.setDialogTitle("Abrir Archivo Escenario");
//            int returnVal2 = filechooser.showOpenDialog(null);
//            if (returnVal2 == JFileChooser.APPROVE_OPTION){
//                File archivo2 = filechooser.getSelectedFile();             
//                
//                
//                try {   //LEYENDO EL ARCHIVO DE ESCENARIO                 
//                                         
//                    lector2 = new FileReader(archivo2);
//                    br2 = new BufferedReader(lector2);
//                    String datos = "";
//                    String dato = "";
//                    while ((datos = br2.readLine()) != null) {
//                        dato += datos + '\n';
//                    }
//                    jTextArea4.setText(dato);
//                    br2.close();
//
//                }catch (Exception ex) {
//                } finally {
//                
//                } 
//            }
//       
//            
//             //CARGANDO EL ARCHIVO DE INTELIGENCIA
//        
//            FileReader lector3 = null;
//            BufferedReader br3 = null;
//            filechooser.setDialogTitle("Abrir Archivo Inteligencia");
//            int returnVal3 = filechooser.showOpenDialog(null);
//            if (returnVal3 == JFileChooser.APPROVE_OPTION) {
//                File archivo3 = filechooser.getSelectedFile();
//             
//                try {
//                     //LEYENDO EL ARCHIVO DE RECURSOS
//                    lector3 = new FileReader(archivo3);
//                    br3 = new BufferedReader(lector3);
//                    String datos = "";
//                    String dato = "";
//                    while ((datos = br3.readLine()) != null) {
//                        dato += datos + '\n';
//                    }
//                    jTextArea5.setText(dato);
//                    br3.close();
//
//                    datos = "";
//                    dato = "";
//                    
//                }catch (Exception ex) {
//                } finally {
//                
//                } 
//            }
        
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed

        JOptionPane.showMessageDialog(this, "ARCHIVOS GUARDADOS EXITOSAMENTE!");
     
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed

//        //GUARDANDO EL ARCHIVO DE RECURSOS
//        
//        if (filechooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
//            String nombrerecursos = filechooser.getSelectedFile().getPath()+".rr";
//    
//            File recursos = new File(nombrerecursos);
//                     try (FileWriter escritor = new FileWriter(recursos)) {
//                             String datos = jTextArea3.getText();
//                             escritor.write(datos);
//                             escritor.close();
//                } 
//             catch (Exception ex) {
//                JOptionPane.showMessageDialog(this, "Error al guardar el archivo! error: "+ex);
//            }
//        }
//        
//        //GUARDANDO EL ARCHIVO ESCENARIO
//        
//        if (filechooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
//            String nombreescenario = filechooser.getSelectedFile().getPath()+".maze";
//            
//            File escenario = new File(nombreescenario);
//                     try (FileWriter escritor = new FileWriter(escenario)) {
//                             String datos = jTextArea4.getText();
//                             escritor.write(datos);
//                             escritor.close();
//                } 
//             catch (Exception ex) {
//                JOptionPane.showMessageDialog(this, "Error al guardar el archivo! error: "+ex);
//            }
//    
//        }
//        
//        //GUARDANDO EL ARCHIVO INTELIGENCIA
//        
//        if (filechooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
//            String nombreinteligencia = filechooser.getSelectedFile().getPath()+".ia";
//            
//            File inteligencia = new File(nombreinteligencia);
//                     try (FileWriter escritor = new FileWriter(inteligencia)) {
//                             String datos = jTextArea5.getText();
//                             escritor.write(datos);
//                             escritor.close();
//                } 
//             catch (Exception ex) {
//                JOptionPane.showMessageDialog(this, "Error al guardar el archivo! error: "+ex);
//            }
//    
//        }
        
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed

        
        JOptionPane.showMessageDialog(this, "Desarrollado por: Felix Alberto Garcia C.");
        JOptionPane.showMessageDialog(this, "Carnet No. 200915675");
        JOptionPane.showMessageDialog(this, "Laboratorio de Compiladores 2");
        
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed

    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jTextArea3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea3KeyPressed


    }//GEN-LAST:event_jTextArea3KeyPressed

    private void jTextArea5KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea5KeyTyped
     
    }//GEN-LAST:event_jTextArea5KeyTyped

    private void jTextArea4KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea4KeyTyped
     
    }//GEN-LAST:event_jTextArea4KeyTyped

    private void jTextArea3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea3KeyTyped
    
    }//GEN-LAST:event_jTextArea3KeyTyped

    private void jTextArea3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextArea3CaretUpdate
        // TODO add your handling code here:
//        int pos  = evt.getDot();
//        
//        try {
//            int row = jTextArea3.getLineOfOffset(pos) + 1;
//            int col = pos - jTextArea3.getLineStartOffset(row - 1) + 1;
//            
//            jLabel1.setText("Fila: " + row + "           Columna: " + col);
//        } catch (BadLocationException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        
//        
 
        
    }//GEN-LAST:event_jTextArea3CaretUpdate

    private void jTextArea4CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextArea4CaretUpdate
//        // TODO add your handling code here:
//        int pos  = evt.getDot();
//        
//        try {
//            int row = jTextArea4.getLineOfOffset(pos) + 1;
//            int col = pos - jTextArea4.getLineStartOffset(row - 1) + 1;
//            
//            jLabel1.setText("Fila: " + row + "           Columna: " + col);
//        } catch (BadLocationException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
    }//GEN-LAST:event_jTextArea4CaretUpdate

    private void jTextArea5CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextArea5CaretUpdate
        // TODO add your handling code here:
//        
//        int pos  = evt.getDot();
//        
//        try {
//            int row = jTextArea5.getLineOfOffset(pos) + 1;
//            int col = pos - jTextArea5.getLineStartOffset(row - 1) + 1;
//            
//            jLabel1.setText("Fila: " + row + "           Columna: " + col);
//        } catch (BadLocationException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_jTextArea5CaretUpdate

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // TODO add your handling code here:
//        File path = new File("MANUAL DE USUARIO PRACTICA1 COMPI1.pdf");
//        try {
//            Desktop.getDesktop().open(path);
//        } catch (IOException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        // TODO add your handling code here:
//        File path = new File("MANUAL TECNICO PRACTICA1 COMPI1.pdf");
//        try {
//            Desktop.getDesktop().open(path);
//        } catch (IOException ex) {
//            Logger.getLogger(ventana_principal.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new principal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JEditorPane jEditorPane2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextArea jTextArea4;
    private javax.swing.JTextArea jTextArea5;
    // End of variables declaration//GEN-END:variables

    

}
