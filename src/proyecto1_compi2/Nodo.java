/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1_compi2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FG250
 */
public class Nodo {
    
    public String tipo;
    public String valor;
    
    public List<Nodo> hijos;
    
    public Nodo(String valor, String tipo) {
        this.valor = valor;
        this.tipo = tipo;
        hijos = new ArrayList<Nodo>();
    }
    
}
