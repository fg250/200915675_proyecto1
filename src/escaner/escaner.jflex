package escaner;
import java_cup.runtime.Symbol;
%%
numero =[0-9]+ "."? [0-9]*
entero = [0-9]+ 
integer = (-)?{numero}
Cadena = [a-zA-Z��]+
Identificador = {Cadena}({Cadena}|{entero}|"_")*
Strng = \" [^\n]+ \"
token = "$"{numero}("." {Identificador})?
Comentario		= "/*" ~ "*/"
Comentario2		= "//"[^\n]*

%cupsym tabla_simbolos
%class lexico
%cup
%public
%unicode
%line
%column
%char
%ignorecase
%%



/* PALABRAS RESERVADAS */
"new" {return new Symbol(tabla_simbolos.tnew, yychar,yyline); }
"break" {return new Symbol(tabla_simbolos.tbreak, yychar,yyline); }
"default" {return new Symbol(tabla_simbolos.tdef, yychar,yyline); }
"continue" {return new Symbol(tabla_simbolos.tcont, yychar,yyline); }
"print" {return new Symbol(tabla_simbolos.tprint, yychar,yyline); }
"go" {return new Symbol(tabla_simbolos.tgo, yychar,yyline); }
"class" {return new Symbol(tabla_simbolos.tclass, yychar,yyline); }
"import" {return new Symbol(tabla_simbolos.timp, yychar,yyline); }
"return" {return new Symbol(tabla_simbolos.treturn, yychar,yyline); }
"if" {return new Symbol(tabla_simbolos.tif, yychar,yyline); }
"else" {return new Symbol(tabla_simbolos.telse, yychar,yyline); }
"for" {return new Symbol(tabla_simbolos.tfor, yychar,yyline); }
"while" {return new Symbol(tabla_simbolos.twhile, yychar,yyline); }
"do" {return new Symbol(tabla_simbolos.tdo, yychar,yyline); }
"repeat" {return new Symbol(tabla_simbolos.trepeat, yychar,yyline); }
"until" {return new Symbol(tabla_simbolos.tuntil, yychar,yyline); }
"loop" {return new Symbol(tabla_simbolos.tloop, yychar,yyline); }
"elseif" {return new Symbol(tabla_simbolos.telseif, yychar,yyline); }
"switch" {return new Symbol(tabla_simbolos.tswitch, yychar,yyline); }
"case" {return new Symbol(tabla_simbolos.tcase, yychar,yyline); }
"pow" {return new Symbol(tabla_simbolos.tpow, yychar,yyline); }
"extends" {return new Symbol(tabla_simbolos.textends, yychar,yyline); }
"public" {return new Symbol(tabla_simbolos.tpub, yychar,yyline); }
"private" {return new Symbol(tabla_simbolos.tpriv, yychar,yyline); }
"protected" {return new Symbol(tabla_simbolos.tprot, yychar,yyline); }
"void" {return new Symbol(tabla_simbolos.tvoid, yychar,yyline); }
"super" {return new Symbol(tabla_simbolos.tsuper, yychar,yyline); }
"this" {return new Symbol(tabla_simbolos.tthis, yychar,yyline); }
"null" {return new Symbol(tabla_simbolos.tnull, yychar,yyline); }
"int" {return new Symbol(tabla_simbolos.tint, yychar,yyline); }
"double" {return new Symbol(tabla_simbolos.tdouble, yychar,yyline); }
"float" {return new Symbol(tabla_simbolos.tfloat, yychar,yyline); }
"string" {return new Symbol(tabla_simbolos.tstring, yychar,yyline); }
"char" {return new Symbol(tabla_simbolos.tchar, yychar,yyline); }
"bool" {return new Symbol(tabla_simbolos.tbool, yychar,yyline); }
"<!Override!>" {return new Symbol(tabla_simbolos.tsobreesc, yychar,yyline); }
"parseint" {return new Symbol(tabla_simbolos.tpint, yychar,yyline); }
"parsedouble" {return new Symbol(tabla_simbolos.tpdoub, yychar,yyline); }
"inttostr" {return new Symbol(tabla_simbolos.tintstr, yychar,yyline); }
"doubletostr" {return new Symbol(tabla_simbolos.tdoubstr, yychar,yyline); }
"doubletoint" {return new Symbol(tabla_simbolos.tdoubint, yychar,yyline); }
"<Dec>" {return new Symbol(tabla_simbolos.tdec, yychar,yyline); }
"<Gram>" {return new Symbol(tabla_simbolos.tgram, yychar,yyline); }
"<Cod>" {return new Symbol(tabla_simbolos.tcod, yychar,yyline); }
"<Terminal>" {return new Symbol(tabla_simbolos.tterm, yychar,yyline); }
"<nonterminal>" {return new Symbol(tabla_simbolos.tnonterm, yychar,yyline); }
"<tipo>" {return new Symbol(tabla_simbolos.ttipo, yychar,yyline); }
"<lista>" {return new Symbol(tabla_simbolos.tlista, yychar,yyline);}
"<nombre>" {return new Symbol(tabla_simbolos.tnombre, yychar,yyline);}
"<precedencia>" {return new Symbol(tabla_simbolos.tprecedencia, yychar,yyline); }
"<asociatividad>" {return new Symbol(tabla_simbolos.tasoc, yychar,yyline); }
"<asociacion>" {return new Symbol(tabla_simbolos.tasociacion, yychar,yyline); }
"<inicio>" {return new Symbol(tabla_simbolos.tinicio, yychar,yyline); }
"<sim>" {return new Symbol(tabla_simbolos.tsim, yychar,yyline); }
"true" {return new Symbol(tabla_simbolos.ttrue, yychar,yyline); }
"false" {return new Symbol(tabla_simbolos.tfalse, yychar,yyline); }
"</Dec>" {return new Symbol(tabla_simbolos.tfindec, yychar,yyline); }
"</Gram>" {return new Symbol(tabla_simbolos.tfingram, yychar,yyline); }
"</Cod>" {return new Symbol(tabla_simbolos.tfincod, yychar,yyline); }
"</Terminal>" {return new Symbol(tabla_simbolos.tfinterm, yychar,yyline); }
"</nonterminal>" {return new Symbol(tabla_simbolos.tfinnonterm, yychar,yyline); }
"</tipo>" {return new Symbol(tabla_simbolos.tfintipo, yychar,yyline); }
"</lista>" {return new Symbol(tabla_simbolos.tfinlista, yychar,yyline);}
"</nombre>" {return new Symbol(tabla_simbolos.tfinnombre, yychar,yyline);}
"</precedencia>" {return new Symbol(tabla_simbolos.tfinprecedencia, yychar,yyline); }
"</asociatividad>" {return new Symbol(tabla_simbolos.tfinasoc, yychar,yyline); }
"</asociacion>" {return new Symbol(tabla_simbolos.tfinasociacion, yychar,yyline); }
"</inicio>" {return new Symbol(tabla_simbolos.tfininicio, yychar,yyline); }
"</sim>" {return new Symbol(tabla_simbolos.tfinsim, yychar,yyline); }


/*
"izq" {return new Symbol(tabla_simbolos.tizq, yychar,yyline); }
"der" {return new Symbol(tabla_simbolos.tder, yychar,yyline); }
"con" {return new Symbol(tabla_simbolos.tcon, yychar,yyline); }
"begin" {return new Symbol(tabla_simbolos.tbegin, yychar,yyline); }
"metod" {return new Symbol(tabla_simbolos.tmetod, yychar,yyline); }
*/

/* OPERADOR */
"++" {return new Symbol(tabla_simbolos.taumento, yychar,yyline); }
"--" {return new Symbol(tabla_simbolos.tdecr, yychar,yyline); }
"+" {return new Symbol(tabla_simbolos.tmas, yychar,yyline); }
"-" {return new Symbol(tabla_simbolos.tmenos, yychar,yyline); }
"/" {return new Symbol(tabla_simbolos.tentre, yychar,yyline); }
"*" {return new Symbol(tabla_simbolos.tpor, yychar,yyline); }
"^" {return new Symbol(tabla_simbolos.tpotencia, yychar,yyline); }
"{" {return new Symbol(tabla_simbolos.tllave1, yychar,yyline); }
"}" {return new Symbol(tabla_simbolos.tllave2, yychar,yyline); }
"[" {return new Symbol(tabla_simbolos.tcorchete1, yychar,yyline); }
"]" {return new Symbol(tabla_simbolos.tcorchete2, yychar,yyline); }
"(" {System.out.println("abre parentesis detectado"); return new Symbol(tabla_simbolos.tparen1, yychar,yyline); }
")" {System.out.println("cierra parentesis detectado"); return new Symbol(tabla_simbolos.tparen2, yychar,yyline); }
"<" {return new Symbol(tabla_simbolos.tmenor, yychar,yyline); }
">" {return new Symbol(tabla_simbolos.tmayor, yychar,yyline); }
"<=" {return new Symbol(tabla_simbolos.tmenorig, yychar,yyline); }
">=" {return new Symbol(tabla_simbolos.tmayorig, yychar,yyline); }
"==" {return new Symbol(tabla_simbolos.tigualig, yychar,yyline); }
"!=" {return new Symbol(tabla_simbolos.tnoigual, yychar,yyline); }
"||" {return new Symbol(tabla_simbolos.tor, yychar,yyline); }
"??" {return new Symbol(tabla_simbolos.txor, yychar,yyline); }
"&&" {return new Symbol(tabla_simbolos.tand, yychar,yyline); }
"!" {return new Symbol(tabla_simbolos.tnot, yychar,yyline); }
":" {System.out.println("dos puntos detectado");return new Symbol(tabla_simbolos.tdospuntos, yychar,yyline); }
";" {System.out.println("punto y coma detectado");return new Symbol(tabla_simbolos.tpcoma, yychar,yyline); }
"," {return new Symbol(tabla_simbolos.tcoma, yychar,yyline); }
"=>" {System.out.println("igual1 detectado");return new Symbol(tabla_simbolos.tigual1, yychar,yyline); }
"=" {System.out.println("igual2 detectado");return new Symbol(tabla_simbolos.tigual2, yychar,yyline); }
"." {return new Symbol(tabla_simbolos.tpunto, yychar,yyline); }
"::=" {return new Symbol(tabla_simbolos.tasig, yychar,yyline); }
"|" {return new Symbol(tabla_simbolos.tpipe, yychar,yyline); }
"$" {return new Symbol(tabla_simbolos.tdolar, yychar,yyline); }
[\"] {return new Symbol(tabla_simbolos.tcomillad, yychar,yyline); }
[\'] {return new Symbol(tabla_simbolos.tcomillas, yychar,yyline); }


{Identificador} {System.out.println("Identificador detectado"); return new Symbol(tabla_simbolos.tid, yychar,yyline,new String(yytext()));}
{integer} 		{System.out.println("Numero detectado"); return new Symbol(tabla_simbolos.tnumero, yychar,yyline,new String(yytext()));}
{Strng}    		{return new Symbol(tabla_simbolos.tstrng, yycolumn,yyline,new String(yytext()));}
{token}			{return new Symbol(tabla_simbolos.ttoken, yychar,yyline,new String(yytext()));}
{Comentario}	{System.out.println("Comentario multilinea detectado!! procediendo a descartar..");}
{Comentario2}	{System.out.println("Comentario de una linea detectado!! procediendo a descartar..");}


/* BLANCOS */
[ \t\r\f\n]+ { /* Se ignoran */}
/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos.terrorlex, yychar,yyline,new String(yytext())); }